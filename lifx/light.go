package lifx

import (
	"fmt"
	"gitlab.com/wwsean08/golifx"
	"strings"
)

func GetLights(labels []string) []*golifx.Device {
	allDevices, err := golifx.LookupDevices()
	if err != nil {
		panic(err)
	}
	resultDevices := make([]*golifx.Device, 0)

	for _, device := range allDevices {
		for _, label := range labels {
			bulbName, err := device.GetLabel()
			if err != nil {
				fmt.Println(err.Error())
			}
			if strings.ToLower(bulbName) == strings.ToLower(label) {
				resultDevices = append(resultDevices, device)
			}
		}
	}
	return resultDevices
}

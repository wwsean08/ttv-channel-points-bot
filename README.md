# ttv-channel-points-bot

This is an example bot that is taking channel points redemptions and then controlling LIFX lights.

This was created for [wwsean08](https://twitch.tv/wwsean08), to allow viewers to control the lights in the room while he streams, but can be used by others to do the same thing.

## Limitations
* If you plan to use the docker image for this, it requires using host networking.  This is because of the way LIFX does discovery of its bulbs is via a network broadcast.  If you are within the docker network, the lightbulbs won't be get the broadcast message.
    * To specify to use the host network include `--net=host` in your `docker run` command 
* If running docker, avoid running latest, that can break at anytime, use one of the vX.Y.Z versions which will not have the issue of changing from under you, and are exempt from pruning.

## Requirements
* A twitch API key with the `channel:read:redemptions` scope

## Usage
The first step regardless of how you run the application is to create your config.  The easiest way to get the channel point reward ID is via your browsers development tools:

![Example of how to get the reward ID](img/reward-id-example.png)

### Docker
**See limitations above for docker**

Find the image you want to run from https://gitlab.com/wwsean08/ttv-channel-points-bot/container_registry/ and then run it.  All docker images are scanned during the build process.  

Example docker run command:
`docker run -it --rm --net=host -v /home/user/.ttv-channel-points-bot.yaml:/.config.yaml registry.gitlab.com/wwsean08/ttv-channel-points-bot:v0.1.0 --config /.config.yaml run`

### Via binary
Binaries can be downloaded a specific build like this and make it executable: 
```
wget https://gitlab.com/wwsean08/ttv-channel-points-bot/-/jobs/561244260/artifacts/raw/ttv-channel-points-bot
chmod +x ttv-channel-points-bot
```

If your config is located at `$HOME/.ttv-channel-points-bot.yaml`, then you can simply run `./ttv-channel-points-bot run` from the directory you downloaded it.  If your config is in a custom location then you would run `./ttv-channel-points-bot --config $CONFIG_LOCATION run`.

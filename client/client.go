package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"time"

	"github.com/lucasb-eyer/go-colorful"
	"github.com/spf13/viper"
	"gitlab.com/wwsean08/go-ttv-pubsub/pkg/topic"
	"gitlab.com/wwsean08/go-ttv-pubsub/pkg/ttvclient"
	"gitlab.com/wwsean08/golifx"
	"gitlab.com/wwsean08/ttv-channel-points-bot/lifx"
)

// Client represents the local twitch client, devices, etc
type Client struct {
	devices           []*golifx.Device
	macToCapabilities map[string]*golifx.DeviceCapabilities
	twitchClient      *ttvclient.Client
	controlData       []lightControlData
}

type lightControlData struct {
	channelPointID string
	color          string
	kelvin         int
	userInput      bool
}

var hexRegex *regexp.Regexp

func init() {
	tmp, err := regexp.Compile("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$")
	if err != nil {
		panic(err)
	}
	hexRegex = tmp
}

// ChannelPointsHandler handles channel point messages
func (c Client) ChannelPointsHandler(msg ttvclient.ChannelPointRedemptionMsg) {
	for _, control := range c.controlData {
		if control.channelPointID == msg.Data.Redemption.Reward.ID {
			if control.userInput {
				userInput := msg.Data.Redemption.UserInput
				// The # prefix is required, add it if it's missing
				userInput = strings.TrimSpace(userInput)
				if userInput[0] != '#' {
					userInput = fmt.Sprintf("#%s", userInput)
				}

				// if it matches, it should be a valid color
				if hexRegex.Match([]byte(userInput)) {
					c.updateLights(userInput, msg.Data.Redemption.Reward.Title, control.kelvin)
					err := fulfill(msg.Data.Redemption.Reward.ID, msg.Data.Redemption.ID)
					if err != nil {
						fmt.Println("Error fulfilling request")
						fmt.Println(err.Error())
					}
				} else {
					err := refund(msg.Data.Redemption.Reward.ID, msg.Data.Redemption.ID)
					if err != nil {
						fmt.Printf("User input %s from %s does not appear to be valid, please redeemChannelPoints manually\n", userInput, msg.Data.Redemption.User.DisplayName)
						fmt.Println(err.Error())
					}
				}
			} else {
				c.updateLights(control.color, msg.Data.Redemption.Reward.Title, control.kelvin)
			}
		}
	}
}

func rewardExists() (bool, error) {
	//curl -X GET 'https://api.twitch.tv/helix/channel_points/custom_rewards?broadcaster_id=274637212'
	//-H 'Client-Id: gx2pv4208cff0ig9ou7nk3riccffxt' \
	//-H 'Authorization: Bearer vjxv3i0l4zxru966wsnwji51tmpkj2'

	channelID := viper.GetInt("channelID")
	authToken := viper.GetString("authToken")
	clientID := viper.GetString("clientID")

	request := http.Request{
		URL: &url.URL{
			Scheme:   "https",
			Host:     "api.twitch.tv",
			Path:     "/helix/channel_points/custom_rewards",
			RawQuery: fmt.Sprintf("broadcaster_id=%d&only_manageable_rewards=true", channelID),
		},
		Method: http.MethodGet,
		Header: map[string][]string{},
	}
	request.Header.Add("client-id", clientID)
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("Authorization", fmt.Sprintf("Bearer %s", authToken))
	client := http.DefaultClient
	resp, err := client.Do(&request)
	if err != nil {
		return false, err
	}
	if resp.StatusCode != 200 {
		return false, fmt.Errorf("invalid response code, expected 200, got %d", resp.StatusCode)
	}
	// find the object
	type reward struct {
		Title string `json:"title"`
	}
	type container struct {
		Data []reward `json:"data"`
	}

	rewards := new(container)
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return false, err
	}
	err = json.Unmarshal(data, rewards)
	if err != nil {
		return false, err
	}
	for _, reward := range rewards.Data {
		if reward.Title == "Change Light Color Using Hex Code" {
			return true, nil
		}
	}

	return false, nil
}

func createReward() error {
	channelID := viper.GetInt("channelID")
	authToken := viper.GetString("authToken")
	clientID := viper.GetString("clientID")
	requestData := map[string]interface{}{
		"title": "Change Light Color Using Hex Code",
		"cost":  500,
		//"background_color": "#392e5c",
		"is_user_input_required": true,
	}
	requestBody, err := json.Marshal(requestData)
	if err != nil {
		return err
	}
	request := http.Request{
		URL: &url.URL{
			Scheme:   "https",
			Host:     "api.twitch.tv",
			Path:     "/helix/channel_points/custom_rewards",
			RawQuery: fmt.Sprintf("broadcaster_id=%d", channelID),
		},
		Method: http.MethodPost,
		Body:   ioutil.NopCloser(bytes.NewReader(requestBody)),
		Header: map[string][]string{},
	}
	fmt.Printf("URL: %s\n", request.URL.String())
	fmt.Printf("Body: %s\n", string(requestBody))
	request.Header.Add("client-id", clientID)
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("Authorization", fmt.Sprintf("Bearer %s", authToken))
	client := http.DefaultClient
	resp, err := client.Do(&request)
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		respBody, _ := ioutil.ReadAll(resp.Body)
		fmt.Printf("%s\n", string(respBody))
		return fmt.Errorf("invalid response code, expected 200, got %d", resp.StatusCode)
	}

	// find the object
	type reward struct {
		Title string `json:"title"`
		ID    string `json:"id"`
	}
	type container struct {
		Data []reward `json:"data"`
	}

	rewards := new(container)
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, rewards)
	if err != nil {
		return err
	}
	for _, reward := range rewards.Data {
		if reward.Title == "Change Light Color Using Hex Code" {
			viper.Set(fmt.Sprintf("channelPoints.%s.userInput", reward.ID), true)
			viper.Set(fmt.Sprintf("channelPoints.%s.kelvin", reward.ID), 3200)
			return viper.WriteConfig()
		}
	}

	return nil
}

func refund(rewardID, redemptionID string) error {
	return redeemChannelPoints(rewardID, redemptionID, true)
}

func fulfill(rewardID, redemptionID string) error {
	return redeemChannelPoints(rewardID, redemptionID, false)
}

func redeemChannelPoints(rewardID, redemptionID string, refund bool) error {
	channelID := viper.GetInt("channelID")
	authToken := viper.GetString("authToken")
	clientID := viper.GetString("clientID")
	requestBody := "{\"status\":\"FULFILLED\"}"
	if refund {
		requestBody = "{\"status\":\"CANCELED\"}"
	}
	request := http.Request{
		URL: &url.URL{
			Scheme:   "https",
			Host:     "api.twitch.tv",
			Path:     "/helix/channel_points/custom_rewards/redemptions",
			RawQuery: fmt.Sprintf("broadcaster_id=%d&reward_id=%s&id=%s", channelID, rewardID, redemptionID),
		},
		Method: http.MethodPatch,
		Body:   ioutil.NopCloser(bytes.NewReader([]byte(requestBody))),
		Header: map[string][]string{},
	}
	request.Header.Add("client-id", clientID)
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("Authorization", fmt.Sprintf("Bearer %s", authToken))
	client := http.DefaultClient
	resp, err := client.Do(&request)
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		return fmt.Errorf("invalid response code, expected 200, got %d", resp.StatusCode)
	}
	return nil
}

// SubHandler handles subscription messages from twitch
func (c Client) SubHandler(msg ttvclient.SubscriptionMsg) {
	plan := msg.SubPlan
	switch plan {
	case "1000":
		c.tier1SubEffect()
	case "Prime":
		c.tier1SubEffect()
	case "2000":
		c.tier2SubEffect()
	case "3000":
		c.tier3SubEffect()
	}
}

func (c Client) tier1SubEffect() {
	mzDevice := new(golifx.Device)
	found := false
	for _, device := range c.devices {
		if c.macToCapabilities[device.MacAddress()].Multizone {
			mzDevice = device
			found = true
			break
		}
	}
	if !found {
		return
	}

	beamScrollLightEffect(mzDevice, "#ff0000")
}

func (c Client) tier2SubEffect() {
	mzDevice := new(golifx.Device)
	found := false
	for _, device := range c.devices {
		if c.macToCapabilities[device.MacAddress()].Multizone {
			mzDevice = device
			found = true
			break
		}
	}
	if !found {
		return
	}

	beamScrollLightEffect(mzDevice, "#00ff00")
}

func (c Client) tier3SubEffect() {
	mzDevice := new(golifx.Device)
	found := false
	for _, device := range c.devices {
		if c.macToCapabilities[device.MacAddress()].Multizone {
			mzDevice = device
			found = true
			break
		}
	}
	if !found {
		return
	}

	beamSparkleEffect(mzDevice, "#ff00ff")
}

func beamScrollLightEffect(mzDevice *golifx.Device, color string) {
	originalZones, _ := mzDevice.GetExtendedColorZones()

	baseHSBK := golifx.HSBK{
		Hue:        originalZones.Colors[0].Hue,
		Saturation: originalZones.Colors[0].Saturation,
		Brightness: 0,
		Kelvin:     3200,
	}

	movingColor, _ := colorful.Hex(color)
	h, s, _ := movingColor.Hsl()
	movingHSBK := golifx.HSBK{
		Hue:        uint16(65535 * float32(h/360)),
		Saturation: uint16(65535 * s),
		Brightness: 13106,
		Kelvin:     3200,
	}
	_ = mzDevice.SetColorState(&baseHSBK, 0)

	newState, _ := mzDevice.GetExtendedColorZones()
	colors := newState.Colors
	golifx.SetTTL(90 * time.Millisecond)
	for i := 0; i < int(originalZones.Count); i++ {
		if i != 0 {
			colors[i-1] = &baseHSBK
		}
		colors[i] = &movingHSBK
		_ = mzDevice.SetExtendedColorZones(colors, 0, 0, golifx.Apply)
	}
	golifx.SetTTL(500 * time.Millisecond)
	_ = mzDevice.SetExtendedColorZones(originalZones.Colors, 0, 0, golifx.Apply)
}

func beamSparkleEffect(mzDevice *golifx.Device, color string) {
	originalZones, _ := mzDevice.GetExtendedColorZones()

	baseHSBK := golifx.HSBK{
		Hue:        originalZones.Colors[0].Hue,
		Saturation: originalZones.Colors[0].Saturation,
		Brightness: 0,
		Kelvin:     3200,
	}

	movingColor, _ := colorful.Hex(color)
	h, s, _ := movingColor.Hsl()
	movingHSBK := golifx.HSBK{
		Hue:        uint16(65535 * float32(h/360)),
		Saturation: uint16(65535 * s),
		Brightness: 656,
		Kelvin:     3200,
	}

	newState, _ := mzDevice.GetExtendedColorZones()
	colors := newState.Colors
	golifx.SetTTL(90 * time.Millisecond)
	for j := 0; j < 20; j++ {
		time.Sleep(500 * time.Millisecond)
		offset := j % 2
		for i := 0; i < int(originalZones.Count); i += 2 {
			if offset == 0 {
				if i < len(colors)-1 {
					colors[i+1] = &baseHSBK
				}
				colors[i] = &movingHSBK
			} else {
				colors[i] = &baseHSBK
				if i < len(colors)-1 {
					colors[i+offset] = &movingHSBK
				}
			}

		}
		golifx.SetTTL(100 * time.Millisecond)
		_ = mzDevice.SetExtendedColorZones(colors, 0, 0, golifx.Apply)
	}
	_ = mzDevice.SetExtendedColorZones(originalZones.Colors, 0, 0, golifx.Apply)
}

func (c Client) updateLights(hexCode, rewardTitle string, kelvin int) {
	color, err := colorful.Hex(hexCode)
	if err != nil {
		fmt.Println(fmt.Sprintf("Error getting color %s for %s, %s", hexCode, rewardTitle, err.Error()))
	}
	h, s, _ := color.Hsl()
	hFloat := float32(h / 360)
	lifxData := golifx.HSBK{
		Hue:        uint16(65535 * hFloat),
		Saturation: uint16(65535 * s),
		Brightness: 65535,
		Kelvin:     uint16(kelvin),
	}
	for _, bulb := range c.devices {
		_ = bulb.SetColorState(&lifxData, 5)
	}
}

func getLightControlData() []lightControlData {
	chanPoints := viper.Sub("channelPoints")
	if chanPoints == nil {
		panic(fmt.Errorf("unable to find channelPoints tag in config"))
	}

	keys := chanPoints.AllKeys()
	tmpKeys := make([]string, 0)
	for _, key := range keys {
		parts := strings.Split(key, ".")
		if isKeyUnique(tmpKeys, parts[0]) {
			tmpKeys = append(tmpKeys, parts[0])
		}
	}
	keys = tmpKeys

	lightControls := make([]lightControlData, 0)
	for _, key := range keys {
		tmp := new(lightControlData)
		tmp.channelPointID = key
		tmp.color = chanPoints.GetString(fmt.Sprintf("%s.color", key))
		tmp.userInput = chanPoints.GetBool(fmt.Sprintf("%s.userInput", key))
		tmp.kelvin = chanPoints.GetInt(fmt.Sprintf("%s.kelvin", key))
		lightControls = append(lightControls, *tmp)
	}
	return lightControls
}

func isKeyUnique(list []string, key string) bool {
	for _, existingKey := range list {
		if key == existingKey {
			return false
		}
	}
	return true
}

// SetupClient sets up the twitch client to control the lights
func SetupClient() {
	exists, err := rewardExists()
	if err != nil {
		fmt.Printf("Error finding reward for custom color: %s\n", err.Error())
	}
	if !exists {
		err := createReward()
		if err != nil {
			fmt.Printf("Error creating reward for custom color: %s\n", err.Error())
		}
	}
	golifx.SetAlwaysBroadcast(true)
	devices := lifx.GetLights(viper.GetStringSlice("bulbNames"))
	macToCapabilities := map[string]*golifx.DeviceCapabilities{}
	for _, device := range devices {
		version, _ := device.GetVersion()
		macToCapabilities[device.MacAddress()] = version.Capabilities
	}
	lightCD := getLightControlData()
	twitchClient, err := ttvclient.CreateClient(viper.GetString("authToken"), ttvclient.TwitchPubSubHost)
	if err != nil {
		panic(err)
	}
	client := Client{
		devices:           devices,
		twitchClient:      twitchClient,
		controlData:       lightCD,
		macToCapabilities: macToCapabilities,
	}
	twitchClient.SetChannelPointRedemptionHandler(client.ChannelPointsHandler)
	twitchClient.SetSubscriptionsHandler(client.SubHandler)
	err = twitchClient.Subscribe([]topic.Topic{
		topic.ChannelPointsRedemption(viper.GetInt("channelID")),
		topic.Subscriptions(viper.GetInt("channelID")),
	})
	if err != nil {
		panic(err)
	}
	println("Running and listening to twitch messages")
}

module gitlab.com/wwsean08/ttv-channel-points-bot

go 1.14

require (
	github.com/lucasb-eyer/go-colorful v1.0.3
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.1
	gitlab.com/wwsean08/go-ttv-pubsub v1.3.1
	gitlab.com/wwsean08/golifx v0.2.1
)

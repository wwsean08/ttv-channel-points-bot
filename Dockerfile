FROM gcr.io/distroless/base
COPY ttv-channel-points-bot /
ENTRYPOINT ["/ttv-channel-points-bot"]
# Run as an unprivileged user
USER 5000